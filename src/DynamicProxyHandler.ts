import ExtendableProxy from './ExtendableProxy';

export default class DynamicProxyHandler implements ProxyHandler<any> {
  public constructor(
    private defaultSettings: any | undefined,
    private builder: (data: any, defaultSettings: any, path: string, fileName?: string) => ExtendableProxy,
    private path: string = '',
    private fileName?: string
  ) {

  }

  public get?(target: any, p: PropertyKey, receiver: any): any {
    const nextPath = `${this.path}.${String(p)}`;
    if (p in target) {
      return this.resolve(target, p, nextPath);
    } else if (this.defaultSettings !== undefined && p in this.defaultSettings) {
      return this.resolve(this.defaultSettings, p, nextPath);
    } else {
      if (this.fileName !== undefined) {
        throw new Error(`Could not resolve property from AppConfig. Property: ${nextPath}, FileName: ${this.fileName}`);
      }
      throw new Error(`Could not resolve property from AppConfig. Property: ${nextPath}`);
    }
  }

  private resolve(target: any, p: PropertyKey, path: string): any {
    const value = target[p];
    const defaultValue = (this.defaultSettings === undefined ? undefined : this.defaultSettings[p]);

    if (Array.isArray(value)) {
      return this.builder(value, defaultValue, path, this.fileName);
    } else if (typeof value === 'object') {
      return this.builder(value, defaultValue, path, this.fileName);
    } else if (typeof value === 'function') {
      return this.builder(value, defaultValue, path, this.fileName);
    } else {
      return value;
    }
  }

}
