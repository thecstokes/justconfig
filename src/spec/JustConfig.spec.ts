import JustConfig from 'JustConfig';

describe('JustConfig', () => {
  describe('fromObject', () => {

    describe('get', () => {
      it('a property', () => {
        const appConfig = JustConfig.fromObject({
          a: 'Hello, World'
        });

        expect(appConfig.a).toEqual('Hello, World');
      });

      it('an object property', () => {
        const appConfig = JustConfig.fromObject({
          a: {
            b: 'Hello, World'
          },
          c: {
            d: {
              e: 'Hello, World'
            }
          }
        });

        expect(typeof appConfig.a === 'object').toBeTruthy();
        expect(Object.keys(appConfig.a)).toContain('b');
      });

      it('an property from an object', () => {
        const appConfig = JustConfig.fromObject({
          a: {
            b: 'Hello, World'
          }
        });

        expect(appConfig.a.b).toEqual('Hello, World');
      });

      it('an property from a deeper object', () => {
        const appConfig = JustConfig.fromObject({
          c: {
            d: {
              e: 'Hello, World'
            }
          }
        });

        expect(appConfig.c.d.e).toEqual('Hello, World');
      });

      it('an array property', () => {
        const appConfig = JustConfig.fromObject({
          a: {
            b: [
              {
                c: 'Hello, World'
              }
            ]
          },
        });

        expect(Array.isArray(appConfig.a.b)).toBeTruthy();
        expect(appConfig.a.b.length).toEqual(1);
        expect(appConfig.a.b[0].c).toEqual('Hello, World');
      });

      it('an array function', () => {
        const appConfig = JustConfig.fromObject({
          a: {
            b: [
              {
                c: 'Hello, World'
              }
            ]
          },
        });

        appConfig.a.b.forEach((x: any) => {
          expect(x.c).toEqual('Hello, World');
        });
      });
    });

    describe('get from default settings', () => {
      it('a property', () => {
        const appConfig = JustConfig.fromObject(
          {},
          {
            a: 'Hello, World'
          });

        expect(appConfig.a).toEqual('Hello, World');
      });

      it('a missing object property', () => {
        const appConfig = JustConfig.fromObject(
          {},
          {
            a: {
              b: 'Hello, World'
            }
          });

        expect(appConfig.a.b).toEqual('Hello, World');
      });

      it('a partially missing object property', () => {
        const appConfig = JustConfig.fromObject(
          {
            a: {}
          },
          {
            a: {
              b: 'Hello, World'
            }
          });

        expect(appConfig.a.b).toEqual('Hello, World');
      });

      it('an index for an empty array', () => {
        const appConfig = JustConfig.fromObject(
          [],
          ['Hello, World']
        );

        expect(Array.isArray(appConfig)).toBeTruthy();
        expect(appConfig.length).toEqual(0);
        expect(appConfig[0]).toEqual('Hello, World');
      });

      it('an index for a non-empty array array', () => {
        const appConfig = JustConfig.fromObject(
          ['Hello, World'],
          ['Hello, World', 'Hello, World2']);

        expect(Array.isArray(appConfig)).toBeTruthy();
        expect(appConfig.length).toEqual(1);
        expect(appConfig[0]).toEqual('Hello, World');
        expect(appConfig[1]).toEqual('Hello, World2');
      });

      it('an array function from an empty array', () => {
        const appConfig = JustConfig.fromObject(
          {
            a: {
              b: []
            },
          },
          {
            a: {
              b: [
                {
                  c: 'Hello, World'
                }
              ]
            },
          });

        appConfig.a.b.forEach((x: any) => {
          expect(x.c).toEqual('Hello, World');
        });
      });

      it('an array function from an empty array', () => {
        const appConfig = JustConfig.fromObject(
          {
            a: {
              b: [
                {
                  c: 'Hello, World'
                }
              ]
            },
          },
          {
            a: {
              b: [
                {
                  c: 'Hello, World'
                },
                {
                  c: 'Hello, World2'
                }
              ]
            },
          });

        appConfig.a.b.forEach((x: any, idx: number) => {
          if (idx === 0) {
            expect(x.c).toEqual('Hello, World');
          } else {
            expect(x.c).toEqual('Hello, World2');
          }
        });
      });
    });
  });

  describe('fromFile', () => {
    it('an array function from an empty array', () => {
      const appConfig = JustConfig.fromFile(
        'SpecData/spec.data.1.json',
        {
          a: {
            b: [
              {
                c: 'Hello, World'
              },
              {
                c: 'Hello, World2'
              }
            ]
          },
        });

      appConfig.a.b.forEach((x: any, idx: number) => {
        if (idx === 0) {
          expect(x.c).toEqual('Hello, World');
        } else {
          expect(x.c).toEqual('Hello, World2');
        }
      });
    });
  });
});
