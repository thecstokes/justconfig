export default class ExtendableProxy {
  [key: string]: any

  constructor(data: any, handler: ProxyHandler<any>) {
    return new Proxy(data, handler);
  }
}
