import * as fs from 'fs';
import * as Path from 'path';
import DynamicProxyHandler from './DynamicProxyHandler';
import ExtendableProxy from './ExtendableProxy';

export default class JustConfig<T = any> extends ExtendableProxy {

  public static fromFile<T = any>(path: string, defaultSettings?: T): JustConfig<T> {
    const data = JSON.parse(fs.readFileSync(Path.resolve(path)).toString());
    return new JustConfig<T>(data, defaultSettings, '', Path.basename(path));
  }

  public static fromObject<T = any>(data: T, defaultSettings?: T): JustConfig<T> {
    return new JustConfig<T>(data, defaultSettings);
  }

  private constructor(data: T, defaultSettings?: T, path: string = '', fileName?: string) {
    const handler = new DynamicProxyHandler(defaultSettings,
      (a, b, c, d) => new JustConfig<any>(a, b, c, d), path, fileName);
    super(data, handler);
  }
}
